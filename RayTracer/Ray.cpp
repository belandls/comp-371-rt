/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a single ray (origin + direction)
*/

#include "Ray.h"

Ray::Ray()
{
}

Ray::Ray(glm::vec3 origin, glm::vec3 direction) : origin(origin), direction(direction)
{
}

//Get linear value from 't'.
glm::vec3 Ray::LinearValue(float t)
{
	return origin + t*direction;
}
