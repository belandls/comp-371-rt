/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a sphere object in a scene
*/

#include "Sphere.h"
#include "ReadingHelper.h"
#include<algorithm>
#include<string>
using namespace std;


Sphere::Sphere()
{
}


Sphere::~Sphere()
{
}

//Load the properties from the scene file
void Sphere::LoadFromFile(std::ifstream & fs)
{
	string tempLine;
	for (int i = 1; i <= 2; i++) {
		getline(fs, tempLine, ':');

		if (tempLine == "pos") {
			_center = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "rad") {
			_radius = ReadingHelper::ExtractFloat(fs);
		}
	}

	ReflectingObject::LoadFromFile(fs);
}

//Solves for 't'
float Sphere::Intersect(Ray ray){

	float b, c, b24c;

	//get b from -b - sqrt(b^2 - 4c)
	b = ray.direction.x * (ray.origin.x - _center.x) + ray.direction.y * (ray.origin.y - _center.y) + ray.direction.z * (ray.origin.z - _center.z);
	b *= 2;
	c = pow(ray.origin.x - _center.x, 2) + pow(ray.origin.y - _center.y, 2) + pow(ray.origin.z - _center.z, 2) - pow(_radius, 2);

	//calculate b^2 - 4c to see if it's positive
	b24c = pow(b, 2) - 4 * c;

	if (b24c > 0){
		float r1, r2;
		//get the two solution
		r1 = (-b + sqrt(b24c)) / 2.0f;
		r2 = (-b - sqrt(b24c)) / 2.0f;
		if (r1 >= 0 && r2 >= 0){
			return min(r1, r2); //get the closest solution for 't'
		}
		else if (r1 < 0 && r2 >= 0){
			return r2;
		}
		else if (r2 < 0 && r1 >= 0){
			return r1;
		}
		
	}

	return numeric_limits<float>::infinity();
}

glm::vec3 Sphere::GetNormal(glm::vec3 point)
{
	return glm::normalize(point - _center);
}
