/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a scene to be rendered
*/

#pragma once
#include <string>
#include <vector>
#include "Camera.h"
#include "ReflectingObject.h"
#include "Light.h"
class Scene
{
public:
	Scene();
	~Scene();

	void LoadFromFile(std::string fn);
	void Trace(std::string destFileName);

private:
	int _height;
	int _width;
	Camera* _cam;
	std::vector<ReflectingObject*> _objects;
	std::vector<Light*> _lights;

	struct Intersection {
		float t;
		ReflectingObject* source;
	};

	Intersection GetClosestIntersection(Ray r, bool shortCircuit = false);

	glm::vec3 SingleTrace(Ray r, int maxDepth, int currentDepth);

};

