/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a plane object in a scene
*/

#include "Plane.h"
#include "ReadingHelper.h"
#include<string>

using namespace std;

Plane::Plane()
{
}


Plane::~Plane()
{
}

//Load properties from file
void Plane::LoadFromFile(std::ifstream & fs)
{
	string temp;
	for (int i = 1; i <= 2; i++) {
		getline(fs, temp, ':');

		if (temp == "nor") {
			_norm = ReadingHelper::ExtractVector(fs);
		}
		else if (temp == "pos") {
			_point = ReadingHelper::ExtractVector(fs);
		}
	}


	ReflectingObject::LoadFromFile(fs);
}

//Solves for 't'
float Plane::Intersect(Ray ray){

	float d = glm::dot(ray.direction, _norm);
	if (abs(d) < 1e-6){ //Check that the ray is not parallel to the plane
		return numeric_limits<float>::infinity();
	}
	else{ //return the 't' to get to the intersection point
		return glm::dot(_point - ray.origin,_norm) / d;
	}

}

glm::vec3 Plane::GetNormal(glm::vec3 point)
{
	return _norm;
}
