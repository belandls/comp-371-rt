/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Static function to help extract data from scene files
*/

#include "ReadingHelper.h"
#include <string>
using namespace std;

//Extracts a vector from the filestream
glm::vec3 ReadingHelper::ExtractVector(std::ifstream & fs)
{
	glm::vec3 retVal;
	string tempComponent;

	fs.ignore(1);
	getline(fs, tempComponent, ' ');
	retVal.x = stof(tempComponent, NULL);

	getline(fs, tempComponent, ' ');
	retVal.y = stof(tempComponent, NULL);

	getline(fs, tempComponent);
	retVal.z = stof(tempComponent, NULL);

	return retVal;
}

//Extracts a vector from the filestream
float ReadingHelper::ExtractFloat(std::ifstream & fs)
{
	float retVal;
	string fTemp;
	getline(fs, fTemp);
	retVal = stof(fTemp, NULL);

	return retVal;
}

//Extract an integer from the filestream
int ReadingHelper::ExtractInt(std::ifstream & fs)
{
	int retVal;
	string iTemp;
	getline(fs, iTemp);
	retVal = stoi(iTemp);

	return retVal;
}
