/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a scene to be rendered
*/

#include "Scene.h"
#include "ReadingHelper.h"
#include "Triangle.h"
#include "Plane.h"
#include "Sphere.h"
#include "CImg.h"
#include  <fstream>
using namespace std;

Scene::Scene()
{
	_cam = NULL;
}


Scene::~Scene()
{
}

//Load the scene from the scene file
void Scene::LoadFromFile(string fn) {

	ifstream sceneFileStream(fn);
	if (sceneFileStream.is_open()) {

		int numOfObjects = ReadingHelper::ExtractInt(sceneFileStream);

		for (int i = 0; i < numOfObjects; ++i) {

			SceneObject* temp = NULL;
			string objName;
			getline(sceneFileStream, objName);

			if (objName == "camera") {
				temp = new Camera();
				_cam = (Camera*)temp;
			}
			else if (objName == "light") {
				temp = new Light();
				_lights.push_back((Light*)temp);
			}
			else {
				if (objName == "triangle") {
					temp = new Triangle();
				}
				else if (objName == "sphere") {
					temp = new Sphere();
				}
				else if (objName == "plane") {
					temp = new Plane();
				}
				_objects.push_back((ReflectingObject*)temp);
			}
			temp->LoadFromFile(sceneFileStream);
		}

		sceneFileStream.close();
	}

	//If a cam was loaded, extract the image width and height
	if (_cam != NULL) {

		_height = _cam->ExtractImageHeight();
		_width = _cam->ExtractImageWidth();

	}

}

//Trace the scene and save it the a file
void Scene::Trace(std::string destFileName)
{
	cimg_library::CImg<float> image(_width, _height, 1, 3, 0.0f);

	//For every pixel
	for (int y = _height - 1; y >= 0; --y) {
		for (int x = 0; x < _width; ++x) {

			int avgCounter = 0;
			glm::vec3 avgColor(0.0f);
			//send multiple rays (For AA)
			for (float yJitter = 0.20f; yJitter <= 0.80f; yJitter += 0.20f) {
				for (float xJitter = 0.20f; xJitter <= 0.80f; xJitter += 0.20f) {
					++avgCounter;
					glm::vec3 pixelColor(0.0f);
					float centeredImageX, centeredImageY;
					//Consider the camera as in the middle of the image plane
					centeredImageX = (float)x - _width / 2.0f + xJitter;
					centeredImageY = (float)y - _height / 2.0f + yJitter;

					Ray r = _cam->GetRayFromImageCoord(centeredImageX, centeredImageY);
					
					//trace the ray up to 4 recursive calls
					avgColor += SingleTrace(r,4,1);
				}
			}

			//average the result from all the ray that were traced
			avgColor /= (float)avgCounter;

			//Update the saved image
			*image.data(x, _height-1 - y, 0, 0) = avgColor.r;
			*image.data(x, _height-1 - y, 0, 1) = avgColor.g;
			*image.data(x, _height-1 - y, 0, 2) = avgColor.b;

		}
	}

	//save the image and show it on screen
	image.normalize(0, 255);
	image.save(destFileName.c_str());
	cimg_library::CImgDisplay disp(image, "lol");
	while (!disp.is_closed());
	disp.wait();


}

//From a ray and the scene objects, get the closest intersection
Scene::Intersection Scene::GetClosestIntersection(Ray r, bool shortCircuit)
{
	Scene::Intersection inter;
	inter.t = numeric_limits<float>::infinity();
	inter.source = NULL;
	for (int i = 0; i < _objects.size(); ++i) {
		float temp = _objects[i]->Intersect(r);
		if (temp > 0.0f && temp < inter.t) {
			inter.t = temp;
			inter.source = _objects[i];
			if (shortCircuit)
				break;
		}
	}

	return inter;
}

//Trace a ray and make it bounce up to maxDepth times.
glm::vec3 Scene::SingleTrace(Ray r, int maxDepth, int currentDepth)
{
	glm::vec3 pixelColor(0.0f); //Final pixel color

	//First get the closest intersection
	Scene::Intersection inter = GetClosestIntersection(r);

	//If the reflected ray intersects with another object and that the maxDEpth is not reached
	if (currentDepth < maxDepth  && inter.t != numeric_limits<float>::infinity() && inter.t > 0) {
		//Get the position of the intersection
		glm::vec3 intersectionPoint = r.LinearValue(inter.t);
		intersectionPoint += inter.source->GetNormal(intersectionPoint)  * (float)8e-4; //add a little bias to avoid self-collision
		//calculate an attenuation factor so that further rays have less impact on the final color
		float att = (float)currentDepth / (float)(maxDepth * 1.5);
		//reflect the ray using recursion
		pixelColor += att * SingleTrace(Ray(intersectionPoint, inter.source->GetReflectedVector(r.direction*-1.0f, intersectionPoint)), maxDepth, currentDepth + 1);
	}

	//Check if the ray actually hits something.
	if (inter.t != numeric_limits<float>::infinity() && inter.t > 0) {

		//If we are treating the closest reflection, than apply the complete phong model
		if (currentDepth == 1) {
			//Get the position of the intersection
			glm::vec3 intersectionPoint = r.LinearValue(inter.t);
			intersectionPoint += inter.source->GetNormal(intersectionPoint)  * (float)8e-4; //add a little bias to avoid self-collision 

			//For each light
			for (int i = 0; i < _lights.size(); ++i) {
				Ray shadowRay(intersectionPoint, _lights[i]->GetDirectionFromPoint(intersectionPoint));
				//Scene::Intersection interTemp = GetClosestIntersection(shadowRay, false);

				/*if (interTemp.t == numeric_limits<float>::infinity() || _lights[i]->IsBeforeIntersection(shadowRay, shadowRay.LinearValue(interTemp.t))) {
					inter.source->ApplyColorContribution(pixelColor, _lights[i]->GetColor(), shadowRay.direction, r.direction * -1.0f, intersectionPoint);
				}*/

				float lightingFactor = 0.0f;
				int visibleCounter = 0;
				int shadowRays = 0;

				//jitter shadow rays towards the light and average the occulsion to get soft shadows
				for (float yJitter = -0.5f; yJitter <= 0.5f; yJitter += 1.0f) {
					for (float xJitter = -0.5f; xJitter <= 0.5f; xJitter += 1.0f) {
						Ray shadowRay(intersectionPoint, _lights[i]->GetDirectionFromPoint(intersectionPoint, xJitter, yJitter));
						Scene::Intersection interTemp = GetClosestIntersection(shadowRay, false);
						++shadowRays;
						//If the ray does not intersec with an object IN FRONT of the light
						if (interTemp.t == numeric_limits<float>::infinity() || _lights[i]->IsBeforeIntersection(shadowRay, shadowRay.LinearValue(interTemp.t))) {
							++visibleCounter; //apply light for this jittered shadow ray
						}
					}
				}
				//Get the amount of lighting that should be applied. Ex. if 50% of the rays were occluded, than only 50% of the light color is considered
				lightingFactor = (float)visibleCounter / (float) shadowRays;
				inter.source->ApplyColorContribution(pixelColor, _lights[i]->GetColor() * lightingFactor, shadowRay.direction, r.direction * -1.0f, intersectionPoint);

				
			}
		}
		//else only apply the ambient color for the reflection
		inter.source->ApplyAmbientColorAndClamp(pixelColor);
	}
	return pixelColor;
}
