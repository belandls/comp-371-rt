/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a light in a scene
*/

#include "Light.h"
#include "ReadingHelper.h"
#include<string>

using namespace std;

Light::Light()
{
}


Light::~Light()
{
}

//Get a direction vector TO the light FROM an arbitrary origin
glm::vec3 Light::GetDirectionFromPoint(glm::vec3 origin)
{
	return glm::normalize(_pos - origin);
}

//Get a direction vector TO the light FROM an arbitrary origin and apply a certain shift (jittering effect)
glm::vec3 Light::GetDirectionFromPoint(glm::vec3 origin, float shiftX, float shiftY)
{
	glm::vec3 temp = _pos;
	temp.x += shiftX;
	temp.y += shiftY;
	//temp.z += shiftY;
	return glm::normalize(temp - origin);
}


bool Light::IsBeforeIntersection(Ray r, glm::vec3 intersectionPoint)
{	
	return glm::distance(r.origin, _pos) < glm::distance(r.origin, intersectionPoint);
}

//Load properties from file
void Light::LoadFromFile(std::ifstream & fs)
{
	string tempLine;
	for (int i = 1; i <= 2; i++) {
		getline(fs, tempLine, ':');

		if (tempLine == "pos") {
			_pos = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "col") {
			_color = ReadingHelper::ExtractVector(fs);
		}
	}

}
