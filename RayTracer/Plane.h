/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a light in a scene
*/

#pragma once
#include "ReflectingObject.h"
#include "ReadingHelper.h"
#include<string>
using namespace std;
class Plane :
	public ReflectingObject
{
public:
	Plane();
	~Plane();

private:
	glm::vec3 _norm; 
	glm::vec3 _point;

	virtual void LoadFromFile(std::ifstream & fs) override;
	virtual float Intersect(Ray ray) override;

	// Inherited via ReflectingObject
	virtual glm::vec3 GetNormal(glm::vec3 point) override;
};

