/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Represents a triangle object in a scene
*/

#include "Triangle.h"
#include "ReadingHelper.h"
#include<string>
using namespace std;


Triangle::Triangle()
{
}


Triangle::~Triangle()
{
}

//Load the properties from the scene file
void Triangle::LoadFromFile(std::ifstream & fs)
{
	string tempLine;
	for (int i = 1; i <= 3; i++) {
		getline(fs, tempLine, ':');

		if (tempLine == "v1") {
			_v1 = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "v2") {
			_v2 = ReadingHelper::ExtractVector(fs);
		}
		else if (tempLine == "v3") {
			_v3 = ReadingHelper::ExtractVector(fs);
		}
	}

	ReflectingObject::LoadFromFile(fs);

	//Get the triangle normal with the vertices value
	_norm = glm::normalize(glm::cross(_v2 - _v1, _v3 - _v1));
}

//Solves for 't'
float Triangle::Intersect(Ray ray){

	//First check if the ray is parallel to the triangle
	float d = glm::dot(ray.direction, _norm);
	if (abs(d) < 1e-6){
		return numeric_limits<float>::infinity();
	}
	else{
		float t = glm::dot(_v1 - ray.origin, _norm) / d;
		glm::vec3 p = ray.origin + t*ray.direction; //"plane" intersection point

		//With the plane intersection point, create vectors from each vertices to that point
		//After, get the cross product of each triangle edge with the adjacent vector the intersection point
		//Do the dot product of this cross product with the triangle's normal
		float d1, d2, d3;
		d1 = glm::dot(_norm, glm::cross(_v2 - _v1, p - _v1));
		d2 = glm::dot(_norm, glm::cross(_v3 - _v2, p - _v2));
		d3 = glm::dot(_norm, glm::cross(_v1 - _v3, p - _v3));

		//If each dot product is psitive, then the intersection is in the triangle.
		if (d1 > 0 && d2 > 0 && d3 > 0){
			return t;
		}
		else{
			return numeric_limits<float>::infinity();
		}

	}


}

glm::vec3 Triangle::GetNormal(glm::vec3 point)
{
	return _norm;
}
