#include "SceneObject.h"
#include "Camera.h"
#include "ReadingHelper.h"
#include "Scene.h"
#include <fstream>
#include <string>

using namespace std;

int main(void){

	Scene s;
	s.LoadFromFile("scene.txt");
	s.Trace("output.bmp");

	return 0;
}