/*
Author : Samuel Beland-Leblanc (27185642)
Purpose : Abstract representation of a scene object.
*/

#pragma once
#include <fstream>
#include "glm.hpp"
class SceneObject
{
public:
	SceneObject();
	~SceneObject();

	virtual void LoadFromFile(std::ifstream &fs) = 0; //delegate loading from file

protected:
	
	
};

